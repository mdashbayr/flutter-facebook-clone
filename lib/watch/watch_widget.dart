import 'package:flutter/material.dart';

class WatchWidget extends StatefulWidget {
  const WatchWidget({Key? key}) : super(key: key);

  @override
  State<WatchWidget> createState() => _WatchWidgetState();
}

class _WatchWidgetState extends State<WatchWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.greenAccent);
  }
}
