import 'package:flutter/material.dart';
import 'package:flutter_facebook_clone/home/home_widget.dart';
import 'package:flutter_facebook_clone/menu/menu_widget.dart';
import 'package:flutter_facebook_clone/styles/my_colors.dart';
import 'package:flutter_facebook_clone/watch/watch_widget.dart';
import 'package:flutter_facebook_clone/friends/friends_widget.dart';
import 'package:flutter_facebook_clone/notifications/notifications_widget.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with WidgetsBindingObserver {
  int _currentIndex = 0;
  final PageController _pageController = PageController(keepPage: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: const [
          HomeWidget(),
          FriendsWidget(),
          WatchWidget(),
          NotificationsWidget(),
          MenuWidget(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0,
        currentIndex: _currentIndex,
        showUnselectedLabels: true,
        selectedItemColor: MyColors.facebookBlue,
        unselectedItemColor: MyColors.navigationBarIcon,
        onTap: (selectedIndex) {
          _currentIndex = selectedIndex;
          _pageController.jumpToPage(selectedIndex);
          setState(() {});
        },
        type: BottomNavigationBarType.fixed,
        selectedLabelStyle: const TextStyle(fontSize: 12),
        unselectedLabelStyle: const TextStyle(fontSize: 12),
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.account_circle_outlined), label: 'Friends'),
          BottomNavigationBarItem(icon: Icon(Icons.live_tv), label: 'Watch'),
          BottomNavigationBarItem(icon: Icon(Icons.notification_important_outlined), label: 'Notifications'),
          BottomNavigationBarItem(icon: Icon(Icons.menu), label: 'Menu'),
        ],
      ),
    );
  }
}
