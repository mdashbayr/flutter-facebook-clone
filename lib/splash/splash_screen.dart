import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_clone/utils/routes.dart';
import 'package:flutter_facebook_clone/styles/my_colors.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  StatelessElement createElement() {
    Future.delayed(const Duration(milliseconds: 1000), () {
      Get.offAllNamed(mainRoute);
    });
    return super.createElement();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.scaffold,
      body: Container(),
    );
  }
}
