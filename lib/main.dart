import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_clone/utils/routes.dart';
import 'package:flutter_facebook_clone/main/main_screen.dart';
import 'package:flutter_facebook_clone/splash/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: splashRoute,
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        splashRoute: (context) => const SplashScreen(),
        mainRoute: (context) => const MainScreen(),
      },
    );
  }
}